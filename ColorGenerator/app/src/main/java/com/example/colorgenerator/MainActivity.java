package com.example.colorgenerator;


import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textViewColor, textViewRed, textViewGreen, textViewBlue, textViewColorCode;
    SeekBar seekBarRed, seekBarGreen, seekBarBlue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewColor = (TextView) findViewById(R.id.textViewColor);
        textViewRed = (TextView) findViewById(R.id.textViewRed);
        textViewGreen = (TextView) findViewById(R.id.textViewGreen);
        textViewBlue = (TextView) findViewById(R.id.textViewBlue);
        textViewColorCode = (TextView) findViewById(R.id.textViewColorCode);

        seekBarRed = (SeekBar) findViewById(R.id.seekBarRed);
        seekBarGreen = (SeekBar) findViewById(R.id.seekBarGreen);
        seekBarBlue = (SeekBar) findViewById(R.id.seekBarBlue);

        seekBarRed.setOnSeekBarChangeListener(onProgressChanged);
        seekBarGreen.setOnSeekBarChangeListener(onProgressChanged);
        seekBarBlue.setOnSeekBarChangeListener(onProgressChanged);
    }

    private final SeekBar.OnSeekBarChangeListener onProgressChanged = new SeekBar.OnSeekBarChangeListener() {
        @SuppressLint({"NonConstantResourceId", "SetTextI18n"})
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            int red = seekBarRed.getProgress();
            int green = seekBarGreen.getProgress();
            int blue = seekBarBlue.getProgress();

            switch (seekBar.getId()) {
                case R.id.seekBarRed:
                    red = progress;
                    textViewRed.setTextColor(Color.rgb(red, 0, 0));
                    seekBar.getProgressDrawable().setColorFilter(Color.rgb(red, 0, 0), PorterDuff.Mode.MULTIPLY);
                    //seekBar.getProgressDrawable().setColorFilter(Color.rgb(red, 0, 0), PorterDuff.Mode.SRC_IN);
                    // Jose Carlos dice que cambiemos esto para no usar PorterDuff pero setColorFilter te pide el Mode
                    //seekBar.getProgressDrawable().setColorFilter(Color.parseColor("#" + Integer.toHexString(red) + "0000"), PorterDuff.Mode.MULTIPLY);
                    textViewRed.setText("Red: " + progress);
                    break;
                case R.id.seekBarGreen:
                    green = progress;
                    textViewGreen.setTextColor(Color.rgb(0, green, 0));
                    seekBar.getProgressDrawable().setColorFilter(Color.rgb(0, green, 0), PorterDuff.Mode.MULTIPLY);
                    textViewGreen.setText("Green: " + progress);
                    break;
                case R.id.seekBarBlue:
                    blue = progress;
                    textViewBlue.setTextColor(Color.rgb(0, 0, blue));
                    seekBar.getProgressDrawable().setColorFilter(Color.rgb(0, 0, blue), PorterDuff.Mode.MULTIPLY);
                    textViewBlue.setText("Blue: " + progress);
                    break;
            }

            textViewColor.setBackgroundColor(Color.rgb(red, green, blue));
            textViewColorCode.setText("#" + Integer.toHexString(red).toUpperCase() + Integer.toHexString(green).toUpperCase() + Integer.toHexString(blue).toUpperCase());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

}