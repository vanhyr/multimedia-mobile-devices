package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> series;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.listViewSeries);

        series = new ArrayList<>();
        series.add("Breaking Bad");
        series.add("Vikings");
        series.add("Sons of Anarchy");
        series.add("Peaky Blinders");
        series.add("Narcos");

        // Default ListView adapter (default on Android)
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, series);
        //listView.setAdapter(adapter);

        // Custom ListView adapter (created by me with images)
        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_item, series);
        listView.setAdapter(myAdapter);

        listView.setOnItemClickListener((adapterView, view, position, id) -> Toast.makeText(MainActivity.this, "You clicked: "+ series.get(position), Toast.LENGTH_SHORT).show());
    }

}