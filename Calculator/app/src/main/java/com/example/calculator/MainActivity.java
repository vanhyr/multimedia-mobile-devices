package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btAdd, btSubtract, btMultiply, btDivide;
    EditText editTextNumberA, editTextNumberB;
    TextView textViewOperation, textViewResult;
    int a, b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btAdd = (Button) findViewById(R.id.btAdd);
        btSubtract = (Button) findViewById(R.id.btSubtract);
        btMultiply = (Button) findViewById(R.id.btMultiply);
        btDivide = (Button) findViewById(R.id.btDivide);

        editTextNumberA = (EditText) findViewById(R.id.editTextNumberA);
        editTextNumberB = (EditText) findViewById(R.id.editTextNumberB);

        textViewOperation = (TextView) findViewById(R.id.textViewOperation);
        textViewResult = (TextView) findViewById(R.id.textViewResult);

        btAdd.setOnClickListener(view -> {
            getValues();
            textViewOperation.setText("+");
            textViewResult.setText(String.valueOf(a + b));
        });

        btSubtract.setOnClickListener(view -> {
            getValues();
            textViewOperation.setText("-");
            textViewResult.setText(String.valueOf(a - b));
        });

        btMultiply.setOnClickListener(view -> {
            getValues();
            textViewOperation.setText("*");
            textViewResult.setText(String.valueOf(a * b));
        });

        btDivide.setOnClickListener(view -> {
            getValues();
            textViewOperation.setText("/");
            if (a > b && b != 0) {
                textViewResult.setText(String.valueOf(a / b));
            } else {
                textViewResult.setText("0");
                Toast.makeText(MainActivity.this, "Can't divide by 0", Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void getValues() {
        a = Integer.parseInt(editTextNumberA.getText().toString());
        b = Integer.parseInt(editTextNumberB.getText().toString());
    }

}