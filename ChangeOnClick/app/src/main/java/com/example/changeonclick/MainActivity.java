package com.example.changeonclick;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    Button btChangeColors;
    ToggleButton tbtChangeColors;
    ConstraintLayout constraintLayout;

    boolean toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btChangeColors = (Button) findViewById(R.id.btChangeColors);
        tbtChangeColors = (ToggleButton) findViewById(R.id.tbtChangeColors);
        constraintLayout = (ConstraintLayout) findViewById(R.id.constraintLayout);

        toggle = true;

        btChangeColors.setOnClickListener(view -> {
            if (toggle) {
                constraintLayout.setBackgroundColor(Color.GRAY);
                btChangeColors.getBackground().setTint(Color.RED);
                btChangeColors.setTextColor(Color.BLACK);
                toggle = false;
            } else {
                constraintLayout.setBackgroundColor(Color.WHITE);
                btChangeColors.getBackground().setTint(Color.BLUE);
                btChangeColors.setTextColor(Color.WHITE);
                toggle = true;
            }
        });

        tbtChangeColors.setOnClickListener(view -> {
            if (tbtChangeColors.getText().equals("Toggle colors")) {
                constraintLayout.setBackgroundColor(Color.WHITE);
                tbtChangeColors.getBackground().setTint(Color.LTGRAY);
                tbtChangeColors.setTextColor(Color.BLACK);
            } else if (tbtChangeColors.getText().equals("Reset colors")) {
                constraintLayout.setBackgroundColor(Color.GRAY);
                tbtChangeColors.getBackground().setTint(Color.RED);
                tbtChangeColors.setTextColor(Color.WHITE);
            }
        });
    }
}