package com.example.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<String> series;

    public MyAdapter(Context context, int layout, ArrayList<String> series) {
        this.context = context;
        this.layout = layout;
        this.series = series;
    }

    @Override
    public int getCount() {
        return this.series.size();
    }

    @Override
    public Object getItem(int position) {
        return this.series.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        // Get the view
        View v = convertView;

        // Create the inflater
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        // Pass the view our inflated list_item.xml layout
        v = layoutInflater.inflate(R.layout.list_item, null);

        // Get the current serie string
        String currentSerie = series.get(position);

        // Fill the textView with the serie string value
        TextView textView = (TextView) v.findViewById(R.id.textView);
        textView.setText(currentSerie);
        // Return the inflated view (custom)
        return v;
    }

}